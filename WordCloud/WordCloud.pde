//Michael Rogers
//ISTA 303
//Courtesy of the the Processing textbook pp 238 -275

String inputTextFile= "cthulhu.txt";
WordFreq table;
PFont tnr;
int N = 150;

void setup() {
  // Input and parse text file
  String [] fileContents = loadStrings(inputTextFile);
  String rawText = join(fileContents, " ").toLowerCase();
  String [] tokens;
  String delimiters = " ,./?<>;:'\"[{]}\\|=+-_()*&^%$#@!∼";
  tokens = splitTokens(rawText, delimiters);
  println(tokens.length+" tokens found in file: "+inputTextFile);
  
  // display stuff
  size(displayWidth, displayHeight-110, OPENGL);
  tnr = createFont("Times New Toman", 120);
  textFont(tnr);
  textSize(24);
  noLoop();
  
  // Create the word frequency table
  table = new WordFreq(tokens);
  println("Max frequency:"+table.maxFreq());
  table.arrange(N);
} // end setup()

void draw(){
  background(255);
  table.display(N);
  table.tabulate(N);
} //end draw()

