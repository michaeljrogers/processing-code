class WordFreq {
  ArrayList<WordTile> wordFrequency;
  String [] stopWords = loadStrings("stopwords.txt");

  WordFreq(String []tokens) {
    wordFrequency = new ArrayList();

    //Compute the wordFrequency table

    for (String t : tokens) {
      if (!_isStopWord(t)) {
        int index = _search(t, wordFrequency);
        if (index>= 0) {
          (wordFrequency.get(index)).incr();
        } else {
          wordFrequency.add(new WordTile(t));
        }
      }
    }//end WordFreq constructor
  }
  void tabulate(int n) {
    println("There are "+N()+" entries.");
    for (int i = 0; i< n; i++) {
      println(wordFrequency.get(i));
    }
  }//end tabulate()
/*
  void arrange(int N) {  // arrange or map the first N tiles in sketch
    WordTile tile; 
    for (int i=0; i < N; i++) {
      tile = wordFrequency.get(i);
      tile.setFontSize();
      tile.setSize();
      tile.setXY(random(width), random(height));
    }
  } // arrange()
*/

  void arrange(int N) {  // arrange or map the first N tiles in sketch
  WordTile tile;
  for (int i=0; i < N; i++) {
    tile = wordFrequency.get(i);
    tile.setFontSize();
    // Exploring the spiral layout
    float cx = width/2, cy = height/2, px, py;
    float R = 0.0, dR = 0.2, theta = 0.0, dTheta = 0.5;
    do {  // find the next x, y for tile, i in spiral
     float x = cx + R*cos(theta);
      float y = cy + R*sin(theta);
      tile.setXY(x, y);
      px = x;
      py = y;
      theta+=dTheta;
      R += dR;
    } // until the tile is clear of all other tiles
    while (!isClear (i));
  }
} // arrange()

  boolean isClear(int n) { // Is tile, i clear of tiles 0..i-1?
    WordTile tile1 = wordFrequency.get(n);
    for (int i=0; i < n; i++) {
      WordTile tile2 = wordFrequency.get(i);
      if (tile1.intersect(tile2)) {
        return false;
      }
    } // for
    return true;
  } // clear()

  void display (int N) {
    for (int i = 0; i < N; i++) {
      WordTile tile = wordFrequency. get(i);
      tile.display();
    }
  }// end dispaly();

  int N() {
    return wordFrequency.size();
  }// end N()

  String [] samples () {
    String [] k = new String[N()];
    int i =0;
    for (WordTile w : wordFrequency) {
      k[i++] = w.getWord();
    }
    return k;
  }// end samples()

  int [] counts() { 
    int [] v = new int[N()];
    int i=0;
    for (WordTile w : wordFrequency) {
      v[i++] = w.getFreq();
    }
    return v;
  }//end counts()

  int maxFreq() {
    return max(counts());
  } //maxFreq()

  int _search(String w, ArrayList<WordTile> L) {
    for (int i = 0; i < L.size (); i++) {
      if (L.get(i).getWord().equals(w))
        return i;
    }
    return -1;
  }// end _search()

  boolean _isStopWord(String word) {
    for (String stopWord : stopWords) {
      if (word.equals(stopWord)) {
        return true;
      }
    }
    return false;
  } // end _isStopWord()
  
 
  String toString() {
    return "Word Frequency Table with "+N()+" entries.";
  }//end toString()
}// end class

