class WordTile extends Word {
  PVector location;
  float tileW, tileH; //width and height
  color tileColor; 
  float tileFS = 24; //font size

  WordTile(String newWord) { // Constructor
    super(newWord);
    setSize();
    location = new PVector(0, 0);
    tileColor = color(0);
  }// end WordTile Constructor

  void setXY (float x, float y) {
    location.x = x; 
    location.y =y;
  }// end setXY()

  void setFontSize() {
    tileFS = map(freq, 1, 40, 10, 120);
    setSize();
  }// end setFontSize()

  void setSize() {
    textSize(tileFS);
    tileW = textWidth(word);
    tileH=textAscent();
  }// end setFileSize()

  void display() {
    fill (tileColor);
    textSize(tileFS);
    text(word, location.x, location.y);
  }// end display()

  boolean intersect(WordTile t2) {
    float left1 = location.x-10;                 
    float right1 = location.x+tileW-10;
    float top1 = location.y-tileH-10;
    float bot1 = location.y-10;
    float left2 = t2.location.x-10;              
    float right2 = left2+t2.tileW-10;
    float bot2 = t2.location.y-10;
    float top2 = bot2-t2.tileH-10;
    return !(right1 < left2 || left1 > right2 || bot1 < top2 || top1 > bot2);  
  } // end intersect()
 
}//end WordTile class

