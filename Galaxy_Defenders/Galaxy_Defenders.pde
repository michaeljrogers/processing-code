int level=10;
int score=1000;
int lives=5;
int background_stars= 3000;
float xStars[]= new float[background_stars];
float yStars[]= new float[background_stars];


void setup() {
  size(700, 700);
  smooth();
  noStroke();
  
  
  PFont font = loadFont("Stencil-30.vlw");
  textFont(font);
  for (int x = 0; x<background_stars; x++) {
    xStars[x]=random(width);
    yStars[x]= random(height);
  }
}

void draw() {
  background(0);
  fill(255);
  rect(0, 0, 700, 75);
  for (int x = 0; x<background_stars; x++) {
    fill(255, 255, 255, random(30, 150));
    ellipse(xStars[x], yStars[x], 2, 2);
  }

  fill(0);
  textSize(50);
  text("GALAXY DEFENDER", 10, 50);
  textSize(25);
  text("Score: "+score, 530, 20);
  text("Level: "+level, 530, 40);
  text("Lives: "+lives, 530, 60);
}

