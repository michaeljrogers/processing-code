float paddleY;
boolean ardInput; //this will be the input variable from arduino.
float direction;
float speed;
int score;


void setup() {
  size(900, 600);
  newGame();
}

void draw() {
  background(0,0,0);
  fill(250, 250, 250);
  rect(0, 0, 40, 600);
  noStroke();
  rect(0, 0, width, 60);
  rect(40, height-20, 860, 20);
  fill(0,0,0);
  textSize(32);
  text(score, width-40, 50);
  fill(250, 250, 250);
  rect(width-40, paddleY, 10, 100);
  if (ardInput) {
    direction= -direction;
    paddleY= paddleY + (direction*speed);
    ardInput=false;
  }
  
  if (paddleY >60 && paddleY<height-120){
      paddleY= paddleY + (direction*speed);
   
  } 
  
}

void newGame() {
  speed=2;
  ardInput=false;
  paddleY=height/2;
  direction =-3;
  score=0;
}

void keyPressed(){
  if (key == ' '){
    ardInput= !ardInput;
  }
}
