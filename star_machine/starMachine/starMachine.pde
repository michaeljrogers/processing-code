//Slider Code--------------------------
import controlP5.*;

ControlP5 cp5;
int myColor = color(0,0,0);
int sliderValue = 100;
int sliderTicks1 = 100;
int sliderTicks2 = 30;
Slider abc;
// end Slider Code----------------


float starDiameter= random(140, 300);
int numPlanets;
color temp;
int background_stars= 3000;

float xStars[] = new float[background_stars];
float yStars[] = new float[background_stars];
float xPos[];
float yPos[];
float distance[];
float angle[];
float size[];
float inc[];
int cv1[];
int cv2[];
int cv3[];

void setup() {
  size(displayWidth, displayHeight);
  noStroke();
  
  fill(250, 0 , 0);
  //Slider Code------------------------------------------------------------------------ 
  cp5 = new ControlP5(this);
  // add a horizontal sliders, the value of this slider will be linked
  // to variable 'sliderValue' 
  cp5.addSlider("sliderValue")
    .setPosition(100, 50)
      .setRange(0, 255)
        ;

  // create another slider with tick marks, now without
  // default value, the initial value will be set according to
  // the value of variable sliderTicks2 then.
  cp5.addSlider("sliderTicks1")
    .setPosition(100, 140)
      .setSize(20, 100)
        .setRange(0, 255)
          .setNumberOfTickMarks(5)
            ;


  // add a vertical slider
  cp5.addSlider("slider")
    .setPosition(100, 305)
      .setSize(200, 20)
        .setRange(0, 200)
          .setValue(128)
            ;
  cp5.getController("slider").getValueLabel().align(ControlP5.LEFT, ControlP5.BOTTOM_OUTSIDE).setPaddingX(0);
  cp5.getController("slider").getCaptionLabel().align(ControlP5.RIGHT, ControlP5.BOTTOM_OUTSIDE).setPaddingX(0);


  cp5.addSlider("sliderTicks2")
    .setPosition(100, 370)
      .setWidth(400)
        .setRange(255, 0) // values can range from big to small as well
          .setValue(128)
            .setNumberOfTickMarks(7)
              .setSliderMode(Slider.FLEXIBLE)
                ;
  // use Slider.FIX or Slider.FLEXIBLE to change the slider handle
  // by default it is Slider.FIX
  //Slider Code----------------------------------------------------------------------------


  numPlanets= (int)random(2, 5);
  xPos= new float[numPlanets];
  yPos= new float[numPlanets];
  distance= new float[numPlanets];
  angle= new float[numPlanets];
  size= new float[numPlanets];
  inc = new float[numPlanets];
  cv1 = new int[numPlanets];
  cv2= new int[numPlanets];
  cv3= new int[numPlanets];
  for (int x = 0; x<background_stars; x++) {
    xStars[x]=random(width);
    yStars[x]= random(height);
  }
  starDiameter= random(140, 300);
  smooth();
  starTemp();
  int initDist= (int)random(starDiameter/2 +200, starDiameter/2 +300); 
  for (int i=0; i<numPlanets; i++) {
    distance[i]=initDist;
    angle[i]=0;
    initDist+= random(60, 300);
    cv1[i]=(int)random(256);
    cv2[i]=(int)random(256);
    cv3[i]=(int)random(256);
    xPos[i] = (int)(cos(angle[i])*(distance[i]/2));
    yPos[i] = (int)(sin(angle[i])*(distance[i]/2));
    size[i] = (int)(random(20, 40));
    inc[i] = 2*PI/distance[i]  ;
  }
}

void draw() {
  background(0, 0, 0);

  for (int x = 0; x<background_stars; x++) {
    fill(255, 255, 255, random(30, 150));
    ellipse(xStars[x], yStars[x], 2, 2);
  }

  fill(temp);
  ellipse(width/2, height/2, starDiameter, starDiameter ); 
 

  for (int i=0; i<numPlanets; i++) {
    angle[i] += inc[i];
    if (angle[i] == 2*PI) {
      angle[i]=0;
    }
    noFill();
    stroke(22, 34, 165);
    ellipse(width/2, height/2, distance[i], distance[i]);
    xPos[i] = (int)(cos(angle[i])*(distance[i]/2)); //This line and the next are the main things I really needed to do research on. However, this equation was so widely proliferated
    yPos[i] = (int)(sin(angle[i])*(distance[i]/2)); //throughout the many various sketches that involve orbiting, I could not say who I should credit. 
    fill(cv1[i], cv2[i], cv3[i]);
    noStroke();
    ellipse(xPos[i]+width/2, yPos[i]+height/2, size[i], size[i]);
    
    
  }
}


void starTemp() { 
  if (starDiameter < 180) {
    temp= color(204, 0, 0) ;
  } else if (starDiameter<220) {
    temp= color (255, 191, 0);
  } else if (starDiameter<260) {
    temp= color (100, 149, 237);
  } else {
    temp= color (255, 0, 0);
  }
}

boolean sketchFullScreen() {
  return true;
}


